Etiquetas Básicas de HTML
=========================

Objetivo:
--------
-Decodificar las etiquetas html de la sesión.

Proceso:
-------

A continuación vamos a explorar una lista de etiquetas HTML que trabajaremos en esta sesión:

`<a>` :

 El elemento `<a>` crea un enlace a otros sitios de internet, archivos o ubicaciones dentro de la misma página, direcciones de correo, o cualquier otra URL.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Segunda Sesión</title>
  </head>
  <body>
    <a href="http://capacitadero.org/">Capacitadero</a>
  </body>
</html>
```

Como vemos, tiene un atributo llamado `href` que contiene una URL o un fragmento de URL al cual apunta el enlace.

Si añadimos `target="_blank"` se abrirá el contenido en una nueva ventana:

`<a href="http://capacitadero.org/" target="_blank">Capacitadero</a>`

`<img>` :

El elemento de imagen HTML `<img>`  representa una imagen en el documento. Para este ejemplo vamos a seleccionar una imagen de nuestra computadora y la guardaremos en la carpeta de trabajo(junto a nuestro archivo HTML). Luego escribiremos el siguiente código:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Segunda Sesión</title>
  </head>
  <body>
    <img src="nombre-de-mi-foto.png" alt="breve descripción de mi imagen">
  </body>
</html>
```

No hay que olvidar que utilizaremos archivos `.png` , `.jpg` o `.gif`  

Aquí vemos los atributos `src` el cual es la URL de la imagen. Este atributo es obligatorio para el elemento `<img>` . Tambien esta `alt` donde se define el texto alternativo que describe la imagen. Los usuarios lo verán si la URL de la imagen es errónea, la imagen tiene un formato no soportado, o si la imagen aun no se ha descargado.

`<ul>` , `<ol>`  y `<li>` :

Los elementos `<ul>` y  `<ol>` abren una lista no ordenada y ordenada, respectivamente. Para su cierre se añade el simbolo ya conocido `/` , es decir `</ul>` y `</ol>` . Usaremos `<li>` para escribir el contenido en cada lista:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Segunda Sesión</title>
  </head>
  <body>
  <!--Etiqueta ul: para lista no ordenada-->
   <ul>
   	<li>Desarrollo Web</li>
   	<li>Código HTML</li>
   	<li>Etiquetas básicas</li>
   </ul>
   <!--Etiqueta ol: para lista ordenada-->
   <ol>
   	<li>Primer texto</li>
   	<li>Segundo texto</li>
   	<li>Tercer texto</li>
   </ol>
  </body>
</html>
```

`<button>` : 

Este elemento crea botones:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Segunda Sesión</title>
  </head>
  <body>
    <button>Ingresar</button>
    <button>Salir</button>
    <button>Publicar</button>
  </body>
</html>
```

`<table>`: 

Nos ayuda a crear una tabla, acontinuación veremos un ejemplo de este elemento con sus componentes:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Segunda Sesión</title>
  </head>
  <body>
    <!-- Tabla Simple -->
	<table>
	  <tr>
	    <td>John</td>
	    <td>Doe</td>
	  </tr>
	  <tr>
	    <td>Jane</td>
	    <td>Doe</td>
	  </tr>
	</table>

<!-- Tabla Simple con dos cabeceras -->
	<table>
	  <tr>
	    <th>First name</th>
	    <th>Last name</th>
	  </tr>
	  <tr>
	    <td>John</td>
	    <td>Doe</td>
	  </tr>
	  <tr>
	    <td>Jane</td>
	    <td>Doe</td>
	  </tr>
	</table>

<!-- Table con thead, tfoot y tbody -->
	<table>
	  <thead>
	    <tr>
	      <th>Header content 1</th>
	      <th>Header content 2</th>
	    </tr>
	  </thead>
	  <tfoot>
	    <tr>
	      <td>Footer content 1</td>
	      <td>Footer content 2</td>
	    </tr>
	  </tfoot>
	  <tbody>
	    <tr>
	      <td>Body content 1</td>
	      <td>Body content 2</td>
	    </tr>
	  </tbody>
	</table>
  </body>
</html>
```

`<input>`: 

Lo utilizaremos para crear las cajas para formularios basados en la web, que reciban información del usuario. La forma en que `<input>` funciona varía considerablemente dependiendo del valor de su atributo `type`.

Algunos ejemplos para el valor del atributo `type` son:

`text` : Campo de texto de línea simple. 

`button` : Botón sin un comportamiento específico.

`checkbox` : Casilla de selección.

`email` : HTML5 Campo para introducir una dirección de correo electrónico.

`password` : Control de línea simple cuyo valor permanece oculto.

`file` : Control que permite al usuario seleccionar un archivo.

`radio` : Botón radio. 

`submit` : Botón que envía el formulario.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Segunda Sesión</title>
  </head>
  <body>
    <input type="text">
    <input type="button">
    <input type="email">
    <input type="password">
    <input type="file">
    <input type="submit">
    <input type="checkbox">
    <input type="radio">
  </body>
</html>
```

### Actividad:

Vamos a empezar a crear nuestro primer sitio web donde utilizaremos todos los elementos aprendidos hoy. A continuación, ingresamos al siguiente enlace del sitio web [Facebook](https://www.facebook.com/) . Vamos a explorar los elementos `HTML` usados por esta empresa y a crear un archivo similar. 

Es útil utilizar las herramientas de nuestros navegadores para explorar su código, por ello, hacemos click derecho sobre el sitio web y seleccionamos `inspeccionar` para ver en un lado todo el código html.

Recursos Adicionales:
--------------------

Parte del texto/imagen del Proceso fue extraido de la organización [Mozilla](https://developer.mozilla.org/es/docs/HTML/HTML5/HTML5_lista_elementos) . Es recomedable revisar el contenido de este enlace como medio de consulta.



