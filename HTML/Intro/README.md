Este es el inicio al Módulo Introductorio de Desarrollo Web
-----------------------------------------------------------

Siempre que empezamos un curso o es nuestro primer dia de clase en el instituto/universidad aparecen muchas preguntas: Qué vamos a lograr? Aprobaremos? Como será el profesor? Quienes serán nuestros nuevos compañeros?

En esta etapa de la Primera Sesión hay que conocernos, compartir nuestra experiencia y cuales son las expectativas o metas que tenemos en la vida.

Paso a paso iremos familiarizándonos con los conceptos, estructuras y buenas prácticas.

Tendremos mucha información en Ingles, sin embargo, no hay que tener miedo. Para eso tenemos el Traductor :)

Empezamos!

Tenemos algunas preguntas que nos ayudarán a entrar en contexto:

Cómo estan elaborados los sitios webs?

Funcionamiento de un Sitio como Facebook.

Qué es HTML? y CSS?

HTML (HyperText Markup Language-Lenguaje de Marcado para Hipertextos) es el código que se usa para crear la estructura de un sitio web.

CSS (Cascading Style Sheets-Hojas de estilo en cascada) son códigos que permiten crear páginas web con un diseño agradable (color, formato,tamaños,etc.)

Que significa Desarrollo Web? y Diseño Web?

Todos juntos intentamos resolver esas preguntas, son lo mismo?

Tienes cursiosidad por saber un poco más?

Aquí hay algunos recursos interesantes:

* https://developer.mozilla.org/es/docs/Learn/HTML/Introduccion_a_HTML
* https://www.w3schools.com/html/default.asp

* https://developer.mozilla.org/es/docs/Learn/CSS/Introduction_to_CSS/Como_funciona_CSS
* https://www.w3schools.com/css/css_intro.asp