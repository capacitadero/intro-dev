Aplicando lo aprendido con Facebook
===================================

Objetivo:
--------
-Aplicar los elementos aprendidos en la sesión anterior, por medio del desarrollo de código HTML similar a un sitio web para fines didácticos.

Proceso:
-------

Hoy empezaremos la sesión aprendiendo más elementos `HTML` que serán necesarios:

`<select>` y `<option>` :

El elemento `<select>` de `HTML` representa un control que muestra un menú de opciones. Las opciones contenidas en el menú son representadas por elementos `<option>`.

Aquí vamos a realizar un ejemplo:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Tercera Sesión</title>
  </head>
  <body>
  	<h1>Perfiles a seleccionar:</h1>
    <select>
	  <option selected>Emprendedor</option> 
	  <option>Tecnológico</option>
	  <option>Marketing</option>
	</select>
  </body>
</html>
```

El atributo `selected` nos ayuda a establer esa opción como seleccionada por defecto.

`<label>` :

Puede estar asociado con un `input` ya sea mediante la utilizacion del atributo `for`(Formulario 1) o ubicándolo dentro del elemento `label`(Formulario 2):

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Tercera Sesión</title>
  </head>
  <body>
  	<h1>Formulario1:</h1>
  	<label for="name">Nombre:</label>
  	<input type="text">
    <h1>Formulario2:</h1>
  	<label>Nombre:<input type="text"/></label>
  </body>
</html>
```

`<div>` :

Viene de "division" -división. Es útil para crear secciones o agrupar contenidos.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Tercera Sesión</title>
  </head>
  <body>
  	<div>
  		<h1>Registrarse:</h1>
  		<p>Nombre de usuario:</p>
  		<input type="text" placeholder="Ingresa tu nickname">
  		<p>Email:</p>
  		<input type="email" placeholder="Ingresa tu email">
  		<p>Password:</p>
  		<input type="password" placeholder="Ingresa tu contraseña">
  		<button>Registrarse</button>
  	</div>
  	<div>
		<h1>Iniciar Sesión:</h1>
  		<input type="email" placeholder="Ingresa tu email">
  		<input type="password" placeholder="Intresa tu contraseña">
  		<button>Ingresar</button>	
  	</div>
  </body>
</html>
```
En esta ocasión, hemos agrupado nuestro código en dos bloques: La `Sección para Registrarse` y la de `Inicio de Sesión`. Más adelante aprenderemos a darle nombre a nuestros `divs` y otros elementos `HTML`.

Tambien estamos añadiendo un atributo adicional a los Inputs de tipo `text`, `email` y `password` llamado `placeholder`:

`placeholder="Ingresa tu email"` nos permite tener un texto dentro del input para indicar al usuario algún detalle sobre el contenido que debe de ingresar. 

### Actividad:

-Ingresamos a [Facebook.com](https://www.facebook.com/) . Vamos a ver el diseño que trabajaremos:

![Diseño principal de Facebook](img/facebook.png)

Y empezamos a crear una estructura `HTML` similar. Podemos revisar la fuente original del código haciendo click derecho en la pantalla de [Facebook.com](https://www.facebook.com/) y seleccionar `inspeccionar` o `inspeccionar elemento`


Recursos Adicionales:
--------------------

Parte del texto/imagen del Proceso fue extraido de la organización [Mozilla](https://developer.mozilla.org/es/docs/HTML/HTML5/HTML5_lista_elementos) . Es recomedable revisar el contenido de este enlace como medio de consulta.
