Construyendo nuestro primer sitio web
=====================================

Objetivo:
--------

-Crear, abrir, editar y guardar archivos html.

-Identificar la estructura de un sitio web.

-Decodificar las etiquetas html de la sesión.

-Crear nuestro "Hola Mundo!" en html.

Proceso:
-------

Crear un archivo HTML es muy sencillo, podemos utilizar el editor de texto que tenemos por defecto en nuestra computadora.

El documento puede guardarse como:  **hola.html**

Anatomia de un Documento HTML:

Todo documento HTML contiene la siguiente estructura básica, la cual trabajaremos durante el taller:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Mi Primera Sesión</title>
  </head>
  <body>
    <p>Hola Mundo!</p>
  </body>
</html>
```
Vemos que hay mucho código que no entendemos, no hay problema, a continuación viene la explicación:

1. `<!DOCTYPE html>`: En los inicios de HTML (1991-92), los doctypes servían como enlaces al conjunto de reglas que la página HTML debía seguir para considerarse buen HTML. En la actualidad se ignora y se considera un legado histórico que hay que incluir para que todo funcione correctamente: <!DOCTYPE html> Todos los sitios webs 
lo utilizan como un estandar, sin embargo, si pruebas en casa todo va a funcionar igual sin él :) 

2. `<html></html>` : El elemento  `html`. Este elemento engloba todo el contenido de la página y es
conocido en ocasiones como el elemento raíz.

3. `<head></head>`: El elemento `head` (cabecera). Este elemento actúa como contenedor de información que quieras incluir en el documento HTML que NO SERÁ visible a los visitantes de la página.

4. `<meta charset="utf-8">`: Este elemento establece que tu documento HTML usará la codificación uft-8, que incluye la gran mayoría de caracteres de todos los lenguajes humanos conocidos. Es decir: puede tratar cualquier contenido de texto que pongas en tu documento. No hay razón para no configurarlo y te puede ayudar a evitar problemas más adelante.

5. `<title></title>`: Este elemento establece el título de tu página, que aparece en la pestaña/ventana de tu navegador cuando la página se carga.

6. `<body></body>` : El elemento `body` contiene todo el contenido que quieres mostrar a los usuarios cuando visitan tu página, ya sea texto, imágenes, vídeos, juegos, pistas de audio reproducibles o cualquier otra cosa.

### Etiquetas Básicas HTML

Ya hemos aprendido la estructura de un sitio web, descubrimos los elementos que lo contienen como
conceptos pero no como se elaboran. A continuación empezaremos a entender un poco más sobre ellos.

Todo elemento dentro de un documento HTML , a excepcion del `<!DOCTYPE html>` y otros casos, tienen una etiqueta de apertura y otra de cierre. Aqui tenemos la imagen de un gato gruñon:

![estructura de la etiqueta p](img/etiqueta.png)

En este ejemplo la etiqueta usada es la "p" y sirve para indicar que el contenido es un párrafo.

Es hora de empezar! Todos juntos vamos a crear un archivo llamado hola.html similar al mostrado anteriormente :)

### Actividad:

Realizaremos juntos un archivo html en donde vamos a escribir que nos motiva aprender código HTML y crear sitios webs.

---
Reto 1 para la siguiente sesión:

Ya hemos aprendido a utilizar la etiqueta p, sin embargo como podemos hacer si queremos tener un titulo en nuestra web? Existe alguna etiqueta que nos pueda ayudar? Sin encontraste alguna, implementala para compartirlo en clase.

---

Reto 2 para la siguiente sesión:

Todas los sitios webs que conocemos tienen enlaces. Cómo podremos añadir un enlace dentro de nuestra web para que el 
usuario al dar click se diriga a `"http://capacitadero.org/"` ?

---

Recursos Adicionales:
--------------------

Parte del texto/imagen del Proceso fue extraido de la organización [Mozilla](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started) siendo adaptado para nuestro objetivo en esta sesión.

* https://developer.mozilla.org/es/docs/Learn/HTML/Introduccion_a_HTML

* https://www.w3schools.com/html/default.asp
