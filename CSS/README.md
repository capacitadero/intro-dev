CSS
====

Descripción:
------------

Módulo Introductorio de CSS dirigido a nuestros estudiantes en Capacitadero. Cualquier principiante que quiera aprender a crear sitios webs puede utilizar los recursos contenidos aquí.

Requisitos Previos:
------------------

A nivel general, no existe requisito previo para el Módulo Introductorio ya que esta dirigido a principiantes.
Utilizaremos un Editor de Texto como Sublime Text, Atom o Bloc de Notas, según la preferencia del estudiante.
Pueden trabajar en cualquier sistema operativo.
Para estudiantes con discapacidad visual es necesario tener experiencia utilizando algún lector de pantalla para computadora: JAWS, NVDA, etc.