Primer paso en CSS
==================

Objetivo:
--------
-Implementar CSS dentro de nuestros archivos HTML.

Proceso:
-------
Aquí empezamos a trabajar con CSS(Cascading Style Sheets-Hojas de estilo en cascada) del que
conversamos al inicio de la primera sesión del Módulo Introductorio.

Hay preguntas que nos planteamos cuando empezamos HTML, seguro se recuerdan de alguna:

¿Cómo puedo tener mi texto rojo o negro?, ¿Cómo hago que mi contenido se muestre en tal y tal lugar de la pantalla? Cómo decoro mi página web con imágenes de fondo y colores?

Para responder a todos estas preguntas tenemos CSS que nos ayudará a darle todos los estilos que queramos a nuestro sitio web.

¿Para qué aprender CSS?

Aprendemos CSS para darle estética, mejor aspecto y color a nuestro sitio web.

### Anatomía:

CSS es un lenguaje de hojas de estilo, es decir, te permite aplicar estilos de manera selectiva a elementos en documentos HTML. Por ejemplo, para seleccionar todos los elementos de párrafo en una página HTML y volver el texto dentro de ellos de color rojo:

![anatomia de css](img/csspartes.png)

Anatomía del ejemplo:

Es importante analizar cada elemento dentro del uso de CSS:

`Selector`:
    El elemento HTML en el que comienza la regla. esta selecciona el(los) elemento(s) a dar estilo (en este caso, los elementos p ). Para dar estilo a un elemento diferente, solo cambia el selector.

`Declaración`:
    Una sola regla como color: red; especifica a cuál de las propiedades del elemento quieres dar estilo.

`Propiedades`:
    Maneras en las cuales puedes dar estilo a un elemento HTML. (En este caso, color es una propiedad del elemento p.) En CSS, seleccionas que propiedad quieres afectar en tu regla.

`Valor de la propiedad`:
    A la derecha de la propiedad, después de los dos puntos (:), tenemos el valor de la propiedad, para elegir una de las muchas posibles apariencias para una propiedad determinada (hay muchos valores para color además de red).    

Nota: Las otras partes importantes de la sintaxis:

    -Cada una de las reglas (aparte del selector) deben estar encapsulada entre corchetes ({}).
    -Dentro de cada declaración, debes usar los dos puntos (:) para separar la propiedad de su valor.
    -Dentro de cada regla, debes usar el punto y coma (;) para separar una declaración de la siguiente.

### Archivos CSS y HTML:

Hay dos formas de trabajar los CSS dentro de nuestros archivos HTML:

Primera forma: Dentro del mismo documento HTML.

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendiendo CSS-Primera forma</title>
    <style>
    	p {
			  color: red;
			}
		h1{color: blue;}
    </style>
  </head>
  <body>
  	<h1>Capacitadero:</h1>
    <p>Aprendiendo en Capacitadero.</p>
  </body>
</html>
```

---

Segunda forma: Trabajando desde otro archivo.

En esta ocasión tendremos dos archivos, un HTML:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendiendo CSS-Segunda forma</title>
    <link href="estilos/miestilo.css" rel="stylesheet" type="text/css">
  </head>
  <body>
  	<h1>Capacitadero:</h1>
    <p>Aprendiendo en Capacitadero.</p>
  </body>
</html>
```
Y el segundo será un archivo CSS, el cual estara dentro de una carpeta nueva llamada `estilos` y su nombre será `miestilo.css`:

```css
p {color: red;}
h1{color: blue;}
```

Guardamos los cambios, actualizamos nuestro `nombre-de-mi-archivo.html` y veremos los cambios.

### Selección Multiple:

Tambien podremos seleccionar otros elementos al mismo tiempo, aqui cambiamos nuestro `CSS` por:

```css
p,h1{color: red;}
```
Para que el título y el párrafo sean del mismo color rojo.

### Selectores:

Existen muchos tipos diferentes de selectores. Hemos visto los selectores de elementos, los cuales seleccionan todos los elementos de un tipo dado en los documentos HTML. Sin embargo podemos hacer selecciones más específicas que esas:

-Selector de elemento(llamado algunas veces selector de etiqueta):

Lo hemos trabajado hoy, selecciona todos los elementos HTML del tipo específico.

`p{ propiedad: valor de la propiedad;}` selecciona `<p>`

-Selector de Clase(`class`):

Los elementos en la página con la clase (`class`) especificada (la clase puede aparecer varias veces en una página).

`.mi-clase{ propiedad: valor de la propiedad;}` seleccionará `<p class="mi-clase">` y `<a class="mi-clase">` .

-Selector de identificación(`id`):

El elemento en la página con el ID específico (en una página HTML dada, solo se permite un único elemento por ID).

`#mi-id{ propiedad: valor de la propiedad;}` seleccionará `<p id="mi-id">` y `<a id="mi-id">` .

---

A continuación tenemos un ejemplo(archivo HTML y otro CSS) con los tres casos aprendidos:

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Aprendiendo CSS-Selectores</title>
    <link href="estilos/miestilo.css" rel="stylesheet" type="text/css">
  </head>
  <body>
  	<h1>Capacitadero:</h1>
    <p class="linea-1">Aprendiendo en Capacitadero.</p>
    <p id="linea-2"> Estamos empezando con CSS</p>
  </body>
</html>
```

```css
h1{color: red;}
.linea-1{color:blue;}
#linea-2{color:yellow;}
```
Guardamos y podremos observar que cada contenido dentro de cada elemento tiene diferente color.

Existen muchos colores y hay otras formas de expresarlos como el [sistema numérico hexadecimal](https://www.w3schools.com/cssref/css_colors.asp)

### Actividad:

Vamos a aplicar todo lo aprendido hoy en nuestro archivo `facebook.html`

Reto: Vamos a organizar nuestro código `HTML` agregando clases que nos ayuden a implementar el CSS necesario.

Recursos Adicionales:
--------------------

Parte del texto/imagen del Proceso fue extraido de la organización [Mozilla](https://developer.mozilla.org/es/docs/Learn/Getting_started_with_the_web/CSS_basics)