Intro-Dev
=========

Capacitadero tiene un Módulo Introductorio donde los estudiantes aprenden conceptos básicos
sobre Internet, código HTML y CSS.
Son 9 sesiones donde los estudiantes, al terminar cada una, deben de elaborar un pequeño proyecto.

Descripción:
-----------

El Módulo Introductorio y el Programa de Capacitadero esta dirigido a estudiantes que tienen alguna discapacidad y quieren aprender sobre Tecnología y Desarrollo web. De igual manera, estos recursos son útiles para cualquier persona que quiera ingresar al increíble mundo tecnológico.

Requisitos Previos:
------------------

A nivel general, no existe requisito previo para el Módulo Introductorio ya que esta dirigido a principiantes.
Utilizaremos un Editor de Texto como Sublime Text, Atom o Bloc de Notas, según la preferencia del estudiante.
Pueden trabajar en cualquier sistema operativo.
Para estudiantes con discapacidad visual es necesario tener experiencia utilizando algún lector de pantalla para computadora: JAWS, NVDA, etc.

Tabla de contenido:
------------------

Contribución: 
------------

Si quieres colaborar con Capacitadero es ideal comunicarte con nosotros porque queremos conocerte! Escribiendo a: capacitadero at gmail dot com

Licencia:
--------

Toda la documentación pertenece a Capacitadero sin embargo, buscamos aportar al mundo contenido constructivo libre de uso. Si utilizas nuestra documentación para fines positivos, estaremos felices.